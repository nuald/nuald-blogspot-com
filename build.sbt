scalatex.SbtPlugin.projectSettings

scalaVersion := "2.12.6"
scalacOptions ++= Seq("-deprecation", "-feature")

libraryDependencies ++= Seq(
  "com.github.scopt" %% "scopt" % "3.7.0",
  "org.clapper" %% "classutil" % "1.3.0",
  "com.lihaoyi" %% "ammonite-ops" % "1.2.1",
  "com.codewaves.codehighlight" % "codehighlight" % "1.0.2"
)

resolvers += Resolver.mavenLocal
