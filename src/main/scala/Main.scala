import java.io._
import java.nio.file.{Files, Path, StandardCopyOption}
import scala.io.Source
import scalatags.Text.all.Frag
import scalatex._
import sys.process._

object Main extends App {

  val Article = Map(
    "GetPid" -> GetPid(),
    "LinuxKit" -> LinuxKit(),
    "LinuxKitCurl" -> LinuxKitCurl(),
    "LambdaSample" -> LambdaSample(),
    "MgmtTactics" -> MgmtTactics(),
    "MgmtTacticsScript" -> MgmtTacticsScript(),
    "CurriculumVitae" -> CurriculumVitae()
  )

  val DefaultHtml = "output.html"

  case class Config(
    mode: String = "",
    input: String = "",
    out: File = new File(DefaultHtml),
    presentation: Boolean = false,
    debug: Boolean = false)

  def write(text: String, file: File): Unit = {
    val writer = new PrintWriter(file)
    writer.write(text)
    writer.close()
  }

  def read(resource: String): String =
    Source.fromResource(resource).getLines.mkString("\n")

  def chrome: Option[String] = {
    val aliases = List(
      "chrome",
      "/Applications/Google\\ Chrome.app/Contents/MacOS/Google\\ Chrome")
    aliases.find(exe => Seq("sh", "-c", s"$exe --headless").! == 0)
  }

  def render(article: Frag, config: Config): Unit = {
    val content = new StringBuilder
    val styles = new StringBuilder
    styles ++= read("hl.css")
    styles ++= read("common.css")
    content ++= article.render
    val gitUrl = "git config --get remote.origin.url".!!.trim.replaceAll("\\.git$", "")
    val fileName = s"${config.input}.scalatex"
    val link = s"$gitUrl/tree/master/src/main/scalatex/$fileName"
    val text = s"""Scalateχ \\src: <a href="$link" target="_blank">$fileName</a>"""
    content ++= s"<pre margin='0.5em'>$text</pre>"

    if (config.presentation) {
      styles ++= read("presentation.css")

      val html = s"<html><head><style type='text/css'>$styles</style></head>" +
        s"<body>$content</body></html>"
      val tmpFile = File.createTempFile(config.out.toString, ".html")
      write(html, tmpFile)
      if (config.debug) {
        println(tmpFile)
      }

      chrome match {
        case Some(exe) =>
          Seq("sh", "-c",
            s"$exe --headless --disable-gpu --print-to-pdf file://$tmpFile").!
          Files.move(new File("output.pdf").toPath, config.out.toPath,
            StandardCopyOption.ATOMIC_MOVE)
        case None =>
          println("Headless Chrome could not be found!")
      }
    } else {
      val html = s"<style type='text/css'>$styles</style>$content"
      write(html, config.out)
    }
  }

  val parser = new scopt.OptionParser[Config]("run.scala") {
    help("help").text("prints this usage text")

    note("\nPlease use one of the commands below:\n")

    opt[Unit]("debug").action((_, c) => c.copy(debug = true)).
          text("print debug information")

    cmd("list").text(" List all avaliable articles:\n").
      action((_, c) => c.copy(mode = "list"))

    cmd("render").text(" Render the article:").
      action((_, c) => c.copy(mode = "render")).
      children(
        opt[File]('o', "out").optional().valueName("<file>").
          action((x, c) => c.copy(out = x)).
          text(s"file to generate ($DefaultHtml by default)"),
        opt[Unit]("presentation").action((_, c) => c.copy(presentation = true)).
          text("generate presentation PDF"),
        arg[String]("<article>...").required().
          action((x, c) => c.copy(input = x)).text("the required article"))
  }
  parser.parse(args, Config()) match {
    case Some(config) => {
      config.mode match {
        case "list" =>
          println(Article.keys.mkString("\n"))
        case "render" => {
          render(Article(config.input), config)
        }
        case _ => parser.showUsage()
      }
    }
    case None =>
  }
}
