package ext

import ammonite.ops._
import scalatags.text.Frag
import scalatags.Text.all._

object hl extends ExtHighlighter {
  override def pathMappings = Seq(
    pwd/'lambda_sample -> "https://github.com/nuald/lambda-sample/tree/master",
    pwd -> "https://github.com/nuald/nuald-blogspot-com/tree/master"
  )

  override def urlMappings = Seq(
    pwd/'lambda_sample -> "https://raw.githubusercontent.com/nuald/lambda-sample/master/",
  )
}

object more extends Frag {
  val v = "<!--more-->"

  def render = v
  def writeTo(strb: StringBuilder) = strb ++= v
}

object page {
  def break = div(cls := "pagebreak")

  def sign(icon: String)(frags: Seq[Frag]) = div(cls := "sign-container")(
    div(icon, cls := "sign-fixed"),
    div(cls := "sign-flex-item")(frags)
  )

  def warning(frags: Frag*) = sign("⚠")(frags)
}
