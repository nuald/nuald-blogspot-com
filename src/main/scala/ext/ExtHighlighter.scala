package ext

import ammonite.ops._
import collection.mutable
import com.codewaves.codehighlight.core._
import com.codewaves.codehighlight.renderer.HtmlRenderer
import scalatags.Text.all._
import scala.io.{BufferedSource, Source}

/**
 * Lets you instantiate a Highlighter object. This can be used to reference
 * snippets of code from files within your project via the `.ref` method, often
 * used via `hl.ref` where `hl` is a previously-instantiated Highlighter.
 */
trait ExtHighlighter { hl =>
  val languages = mutable.Set.empty[String]
  def style: String = "idea"

  case class lang(name: String) {
    def apply(s: Any*) = hl.highlight(s.mkString, name)
  }

  def as = lang("actionscript")
  def bash = lang("bash")
  def scala = lang("scala")
  def asciidoc = lang("asciidoc")
  def ahk = lang("autohotkey")
  def sh = lang("shell")
  def clj = lang("clojure")
  def coffee = lang("coffeescript")
  def ex = lang("elixir")
  def erl = lang("erlang")
  def fs = lang("fsharp")
  def hs = lang("haskell")
  def hx = lang("haxe")
  def js = lang("javascript")
  def nim = lang("nimrod")
  def rb = lang("ruby")
  def ts = lang("typescript")
  def vb = lang("vbnet")
  def xml = lang("xml")
  def diff = lang("diff")

  /**
   * A mapping of file-path-prefixes to URLs where the source
   * can be accessed. e.g.
   *
   * Seq(
   *   "clones/scala-js" -> "https://github.com/scala-js/scala-js/blob/master",
   *   "" -> "https://github.com/lihaoyi/scalatex/blob/master"
   * )
   *
   * Will link any code reference from clones/scala-js to the scala-js
   * github repo, while all other paths will default to the scalatex
   * github repo.
   *
   * If a path is not covered by any of these rules, no link is rendered
   */
  def pathMappings: Seq[(Path, String)] = Nil

  def urlMappings: Seq[(Path, String)] = Nil

  /**
   * A mapping of file name suffixes to highlight.js classes.
   * Usually something like:
   *
   * Map(
   *   "scala" -> "scala",
   *   "js" -> "javascript"
   * )
   */
  def suffixMappings: Map[String, String] = Map(
    "scala" -> "scala",
    "sbt" -> "scala",
    "scalatex" -> "scala",
    "as" -> "actionscript",
    "ahk" -> "autohotkey",
    "coffee" -> "coffeescript",
    "clj" -> "clojure",
    "cljs" -> "clojure",
    "sh" -> "bash",
    "ex" -> "elixir",
    "erl" -> "erlang",
    "fs" -> "fsharp",
    "hs" -> "haskell",
    "hx" -> "haxe",
    "js" -> "javascript",
    "nim" -> "nimrod",
    "rkt" -> "lisp",
    "scm" -> "lisp",
    "sch" -> "lisp",
    "rb" -> "ruby",
    "ts" -> "typescript",
    "vb" -> "vbnet"
  )

  def highlightedCode(lang: String, blob: String) = {
    val renderedFactory = new StyleRendererFactory() {
      def create(languageName: String) = new HtmlRenderer("hljs-")
    }

    val staticHighlighter = new Highlighter(renderedFactory)
    val result = staticHighlighter.highlight(lang, blob)
    raw(result.getResult().toString)
  }

  /**
   * Highlight a short code snippet with the specified language
   */
  def highlight(string: String, lang: String) = {
    languages.add(lang)
    val lines = string.split("\n", -1)
    if (lines.length == 1) {
      code(
        cls:=lang + " hljs",
        display:="inline",
        padding:=0,
        margin:=0,
        highlightedCode(lang, lines(0))
      )
    } else {
      val minIndent = lines.filter(_.trim != "").map(_.takeWhile(_ == ' ').length).min
      val stripped = lines.map(_.drop(minIndent))
        .dropWhile(_ == "")
        .mkString("\n")

      pre(code(cls:=lang + " hljs", highlightedCode(lang, stripped)))
    }
  }

  import ExtHighlighter._

  /**
   * Grab a snippet of code from the given filepath, and highlight it.
   *
   * @param filePath The file containing the code in question
   * @param start Snippets used to navigate to the start of the snippet
   *              you want, from the beginning of the file
   * @param end Snippets used to navigate to the end of the snippet
   *            you want, from the start of start of the snippet
   * @param className An optional css class set on the rendered snippet
   *                  to determine what language it gets highlighted as.
   *                  If not given, it defaults to the class given in
   *                  [[suffixMappings]]
   */
  def ref[S: RefPath, V: RefPath](
    filePath: ammonite.ops.BasePath,
    start: S = Nil,
    end: V = Nil,
    className: String = null,
    skipLink: Boolean = false
  ) = {
    val absPath = filePath match {
      case p: Path => p
      case p: RelPath => pwd/p
      case _ => throw new RefError(s"Highlighter unable to resolve $filePath")
    }

    val ext = filePath.last.split('.').last
    val lang = Option(className).getOrElse(suffixMappings.getOrElse(ext, ext))

    val linkData = pathMappings.iterator.find {
      case (prefix, path) => absPath startsWith prefix
    }

    val content = if (exists! absPath) {
      read.lines! absPath
    } else {
      val urlData = urlMappings.iterator.find {
        case (prefix, _) => absPath startsWith prefix
      }
      val urlOpt = urlData.map {
        case (prefix, url) => s"$url/${absPath relativeTo prefix}"
      }
      urlOpt match {
        case Some(url) => Source.fromURL(url).getLines.toIndexedSeq
        case None => throw new RefError(s"Highlighter unable to get URL for $filePath")
      }
    }

    val (startLine, endLine, blob) = referenceText(content, start, end)
    val link = linkData.map { case (prefix, url) =>
      val hash = if (endLine == -1 || (start == Nil && end == Nil)) {
        ""
      } else {
        s"#L$startLine-L$endLine"
      }

      val linkUrl = s"$url/${absPath relativeTo prefix}$hash"
      p(
        "\\src: ",
        margin:="0.5em",
        a(
          absPath.last,
          href:=linkUrl,
          target:="_blank"
        )
      )
    }

    pre(
      if (skipLink) "" else link,
      code(cls:=lang + " hljs", highlightedCode(lang, blob))
    )
  }

  def referenceText[S: RefPath, V: RefPath](fileLines: IndexedSeq[String], start: S, end: V) = {
    // Start from -1 so that searching for things on the first line of the file (-1 + 1 = 0)

    def walk(query: Seq[String], start: Int) = {
      var startIndex = start
      for (str <- query) {
        startIndex = fileLines.indexWhere(_.contains(str), startIndex + 1)
        if (startIndex == -1) throw new RefError(
          s"Highlighter unable to resolve reference $str in selector $query"
        )
      }
      startIndex
    }

    // But if there are no selectors, start from 0 and not -1
    val startQuery = implicitly[RefPath[S]].apply(start)
    val startIndex = if (startQuery == Nil) 0 else walk(startQuery, -1)
    val startIndent = fileLines(startIndex).takeWhile(_.isWhitespace).length
    val endQuery = implicitly[RefPath[V]].apply(end)
    val endIndex = if (endQuery == Nil) {
      val next = fileLines.drop(startIndex).takeWhile { line =>
        line.trim == "" || line.takeWhile(_.isWhitespace).length >= startIndent
      }
      startIndex + next.length
    } else {
      walk(endQuery, startIndex)
    }
    val margin = fileLines(startIndex).takeWhile(_.isWhitespace).length
    val lines = fileLines.slice(startIndex, endIndex)
                   .map(_.drop(margin))
                   .reverse
                   .dropWhile(_.trim == "")
                   .reverse

    (startIndex, endIndex, lines.mkString("\n"))
  }
}

object ExtHighlighter {
  class RefError(msg: String) extends Exception(msg)

  /**
   * A context bound used to ensure you pass a `String`
   * or `Seq[String]` to the `@hl.ref` function
   */
  trait RefPath[T]{
    def apply(t: T): Seq[String]
  }

  object RefPath{
    implicit object StringRefPath extends RefPath[String] {
      def apply(t: String): Seq[String] = Seq(t)
    }

    implicit object SeqRefPath extends RefPath[Seq[String]] {
      def apply(t: Seq[String]): Seq[String] = t
    }

    implicit object NilRefPath extends RefPath[Nil.type] {
      def apply(t: Nil.type): Seq[String] = t
    }
  }
}
