#!/usr/bin/env scalas

/***
scalaVersion := "2.12.8"
libraryDependencies += "net.java.dev.jna" % "jna-platform" % "5.2.0"
*/

import com.sun.jna.platform.win32.Kernel32
import com.sun.jna.platform.win32.WinNT.HANDLE
import com.sun.jna.Pointer
import sys.process.Process

def getPrivateLongField(proc: Any, name: String): Long = {
  val privateField = proc.getClass.getDeclaredField(name)
  privateField.synchronized {
    privateField.setAccessible(true)
    try {
      privateField.getLong(proc)
    } finally {
      privateField.setAccessible(false)
    }
  }
}

def pidJava(proc: Any): Long = {
  proc match {
    case unixProc: Any
      if unixProc.getClass.getName == "java.lang.UNIXProcess" => {
        getPrivateLongField(unixProc, "pid")
      }
    case windowsProc: Any
      if windowsProc.getClass.getName == "java.lang.ProcessImpl" => {
        val processHandle = getPrivateLongField(windowsProc, "handle")
        val kernel = Kernel32.INSTANCE
        val winHandle = new HANDLE()
        winHandle.setPointer(Pointer.createConstant(processHandle))
        kernel.GetProcessId(winHandle)
      }
    case _ => throw new RuntimeException(
      "Cannot get PID of a " + proc.getClass.getName)
  }
}

def pid(p: Process): Long = {
  val procField = p.getClass.getDeclaredField("p")
  procField.synchronized {
    procField.setAccessible(true)
    val proc = procField.get(p)
    try {
      pidJava(proc)
    } finally {
      procField.setAccessible(false)
    }
  }
}

val processName = "ping -c1 8.8.8.8"
val proc = Process(processName).run
val id = pid(proc)
println(s"Process '$processName' has PID: $id")
