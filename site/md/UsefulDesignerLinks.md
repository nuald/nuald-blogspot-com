One of the major problems in creating a GUI prototype for some websites or applications is developing an initial design. To make the task a little bit less complicated I've listed the websites where you can get templates and pictures for free:

- <https://material.io/resources/icons/> - symbols for common actions and items;
- <https://www.openwebdesign.org/> - many templates for websites available by various licenses;
- <http://www.iconarchive.com/> - many icons for web and other applications available by various licenses;
- <https://www.clipsafari.com/> - many icons and other images distributed under the [CC0](https://creativecommons.org/publicdomain/zero/1.0/) "No Rights Reserved" license. You can copy, modify, and distribute these images, even for commercial purposes, all without asking permission.

Also there are certain standards which are highly recommended to developers to follow:

- [Material Design](https://material.io/design/) principles by Google;
- [GNOME Human Interface Guidelines](https://developer.gnome.org/hig/stable/) describe ways to create GNOME applications;
- [Design applications for the Windows desktop](https://docs.microsoft.com/en-us/windows/apps/desktop/);
- [Java Look and Feel Design Guidelines](http://www.oracle.com/technetwork/java/jlf-135985.html) describe ways to create applications in Java framework;
- [KDE User Interface Guidelines](https://hig.kde.org/) describe ways to create applications for KDE.

There are many other resources available in the Internet, so happy googling!
