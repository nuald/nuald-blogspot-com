#[macro_use]
extern crate clap;

use clap::Arg;
use pulldown_cmark::{html, Options, Parser};
use std::fs;
use std::path::Path;

fn main() -> Result<(), Box<dyn std::error::Error + 'static>> {
    let m = app_from_crate!()
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input file to use")
                .required(true),
        )
        .arg(
            Arg::with_name("OUTPUT")
                .short("o")
                .long("out")
                .default_value("output.html")
                .help("Sets the output file")
                .takes_value(true),
        )
        .get_matches();

    let input = m.value_of("INPUT").unwrap();
    let output = m.value_of("OUTPUT").unwrap();

    let markdown_input = fs::read_to_string(input)?;
    let parser = Parser::new_ext(&markdown_input, Options::empty());

    let mut html_output = "<style type='text/css'>".to_string();
    html_output.push_str(&fs::read_to_string(Path::new("res").join("common.css"))?);
    html_output.push_str("</style>");
    html::push_html(&mut html_output, parser);
    fs::write(output, html_output)?;

    Ok(())
}
