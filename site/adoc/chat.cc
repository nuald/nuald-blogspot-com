#include <arpa/inet.h>
#include <atomic>
#include <cassert>
#include <condition_variable>
#include <iomanip>
#include <iostream>
#include <map>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/x509.h>
#include <signal.h>
#include <string.h>
#include <thread>
#include <unistd.h>
#include <vector>

// Network settings
const auto MADDR = inet_addr("224.0.0.100");
const auto PORT = 6543;

// Multi-threading sync objects
std::condition_variable listener_ready;
std::atomic<bool> is_listening(true);
std::atomic<bool> resend_public_key(false);

// Global configuration
bool verbose = false, encryption = false;

// Public keys registry
std::map<std::string, std::string> keys;

// String functions
namespace string {
  void dump(const std::string &title, const std::string& data) {
    auto i = 0;
    std::cout << title << std::endl << std::setfill('0') << "   ";

    for (const auto& c: data) {
      std::cout << std::hex << std::setw(2) << (int)(uint8_t)c
                << (++i % 16 == 0 ? "\n   " : " ");
    }

    std::cout << std::endl;
  }

  std::vector<std::string> split(const std::string& data, char delimiter) {
    std::stringstream input_stringstream(data);
    std::vector<std::string> result;

    std::string token;
    while (getline(input_stringstream, token, delimiter)) {
      if (!token.empty()) {
        result.push_back(token);
      }
    }

    return result;
  }

  template<typename T>
  std::string concat(T v) {
    std::stringstream ss;
    ss << v;
    return ss.str();
  }

  template<typename T, typename... Args>
  std::string concat(T first, Args... args) {
    return concat(first) + concat(args...);
  }
}

// Base64 functions (using libcrypto)
namespace base64 {
  // tag::base64[]
  std::string encode(const std::string& data) {
    std::shared_ptr<BIO> b64(BIO_new(BIO_f_base64()), BIO_free_all);
    BIO_set_flags(b64.get(), BIO_FLAGS_BASE64_NO_NL);
    auto bio = BIO_new(BIO_s_mem());
    BIO_push(b64.get(), bio);
    BIO_write(b64.get(), data.c_str(), data.size());
    BIO_flush(b64.get());

    char *str;
    auto size = BIO_get_mem_data(bio, &str);
    return std::string(str, size);
  }

  std::string decode(const std::string& data) {
    std::shared_ptr<BIO> b64(BIO_new(BIO_f_base64()), BIO_free_all);
    BIO_set_flags(b64.get(), BIO_FLAGS_BASE64_NO_NL);
    auto bio = BIO_new_mem_buf(data.c_str(), data.size());
    BIO_push(b64.get(), bio);
    std::shared_ptr<BIO> bio_out(BIO_new(BIO_s_mem()), BIO_free_all);

    char inbuf[512];
    int inlen;
    while ((inlen = BIO_read(b64.get(), inbuf, sizeof(inbuf))) > 0 ) {
      BIO_write(bio_out.get(), inbuf, inlen);
    }
    BIO_flush(bio_out.get());

    char *str;
    auto size = BIO_get_mem_data(bio_out.get(), &str);
    return std::string(str, size);
  }
  // end::base64[]
}

// libcrypto functions
namespace crypto {
  void _(const std::string &op, int code) {
    if (code <= 0) {
      std::cerr << op << " failed with the error:" << std::endl;
      ERR_print_errors_fp(stderr);
      exit(EXIT_FAILURE);
    }
  }

  template<typename T> T* _(const std::string &op, T* ptr) {
    if (!ptr) {
      _(op, 0);
    }
    return ptr;
  }

  // tag::generate_pair[]
  std::shared_ptr<EVP_PKEY> generate_pair(int nid) {
    std::shared_ptr<EVP_PKEY_CTX>
      pctx(_("EVP_PKEY_CTX_new_id",
             EVP_PKEY_CTX_new_id(EVP_PKEY_EC, nullptr)),
           EVP_PKEY_CTX_free);

    _("EVP_PKEY_paramgen_init", EVP_PKEY_paramgen_init(pctx.get()));

    _("EVP_PKEY_CTX_set_ec_paramgen_curve_nid",
      EVP_PKEY_CTX_set_ec_paramgen_curve_nid(pctx.get(), nid));

    EVP_PKEY *params_ptr = nullptr;
    _("EVP_PKEY_paramgen", EVP_PKEY_paramgen(pctx.get(), &params_ptr));
    std::shared_ptr<EVP_PKEY> params(params_ptr, EVP_PKEY_free);

    std::shared_ptr<BIO> out(BIO_new_fp(stdout, BIO_NOCLOSE), BIO_free);
    if (verbose) {
      std::cout << "Private key information:" << std::endl;
      EVP_PKEY_print_private(out.get(), params.get(), 3, NULL);
      std::cout << "Public key information:" << std::endl;
      EVP_PKEY_print_public(out.get(), params.get(), 3, NULL);
    }

    std::shared_ptr<EVP_PKEY_CTX>
      kctx(_("CLT: EVP_PKEY_CTX_new",
             EVP_PKEY_CTX_new(params.get(), nullptr)),
           EVP_PKEY_CTX_free);

    _("EVP_PKEY_keygen_init", EVP_PKEY_keygen_init(kctx.get()));

    EVP_PKEY *pkey_ptr = nullptr;
    _("EVP_PKEY_keygen", EVP_PKEY_keygen(kctx.get(), &pkey_ptr));
    return std::shared_ptr<EVP_PKEY>(pkey_ptr, EVP_PKEY_free);
  }
  // end::generate_pair[]

  // tag::extract_public_key[]
  std::string extract_public_key(const std::shared_ptr<EVP_PKEY> &pkey) {
    std::shared_ptr<BIO> bio(BIO_new(BIO_s_mem()), BIO_free_all);
    _("i2d_PUBKEY_bio", i2d_PUBKEY_bio(bio.get(), pkey.get()));

    char *str;
    auto size = BIO_get_mem_data(bio.get(), &str);

    std::string public_key(str, size);
    if (verbose) {
      string::dump("Raw public key:", public_key);
    }
    return public_key;
  }
  // end::extract_public_key[]

  // tag::derive_key[]
  std::string derive_key(const std::shared_ptr<EVP_PKEY> pkey,
                         const std::string &public_key) {
    typedef std::vector<unsigned char> uc_vector;

    std::shared_ptr<BIO> decoded(BIO_new(BIO_s_mem()), BIO_free_all);
    BIO_write(decoded.get(), public_key.c_str(), public_key.size());

    EVP_PKEY *peerkey_ptr = nullptr;
    _("d2i_PUBKEY_bio", d2i_PUBKEY_bio(decoded.get(), &peerkey_ptr));
    std::shared_ptr<EVP_PKEY> peerkey(peerkey_ptr, EVP_PKEY_free);

    std::shared_ptr<EVP_PKEY_CTX>
      ctx(_("SRV: EVP_PKEY_CTX_new",
            EVP_PKEY_CTX_new(pkey.get(), nullptr)),
          EVP_PKEY_CTX_free);

    _("EVP_PKEY_derive_init", EVP_PKEY_derive_init(ctx.get()));

    _("EVP_PKEY_derive_set_peer",
      EVP_PKEY_derive_set_peer(ctx.get(), peerkey.get()));

    size_t secret_len = 0;
    _("EVP_PKEY_derive length",
      EVP_PKEY_derive(ctx.get(), nullptr, &secret_len));
    uc_vector secret(secret_len);
    _("EVP_PKEY_derive",
      EVP_PKEY_derive(ctx.get(), &secret[0], &secret_len));

    uc_vector hash(SHA256_DIGEST_LENGTH);
    SHA256(&secret[0], secret_len, &hash[0]);
    return std::string(hash.begin(), hash.end());
  }
  // end::derive_key[]

  // tag::aes[]
  std::string encrypt(const std::string& plaintext,
                      const EVP_CIPHER *cipher,
                      const std::string& key,
                      const std::string& iv) {
    typedef std::vector<unsigned char> uc_vector;

    std::shared_ptr<EVP_CIPHER_CTX> ctx(_("EVP_CIPHER_CTX_new",
                                          EVP_CIPHER_CTX_new()),
                                        EVP_CIPHER_CTX_free);

    uc_vector uc_key(key.begin(), key.end());
    uc_vector uc_iv(iv.begin(), iv.end());
    _("EVP_EncryptInit_ex",
      EVP_EncryptInit_ex(ctx.get(), cipher, nullptr,
                         &uc_key[0], &uc_iv[0]));

    int len = plaintext.size() + EVP_CIPHER_block_size(cipher);
    uc_vector ciphertext(len);
    uc_vector uc_plaintext(plaintext.begin(), plaintext.end());
    _("EVP_EncryptUpdate",
      EVP_EncryptUpdate(ctx.get(),
                        &ciphertext[0], &len,
                        &uc_plaintext[0], uc_plaintext.size()));

    int tmplen = 0;
    _("EVP_EncryptFinal_ex",
      EVP_EncryptFinal_ex(ctx.get(), &ciphertext[len], &tmplen));

    ciphertext.resize(len + tmplen);
    return std::string(ciphertext.begin(), ciphertext.end());
  }

  std::string decrypt(const std::string& ciphertext,
                      const EVP_CIPHER *cipher,
                      const std::string& key,
                      const std::string& iv) {
    typedef std::vector<unsigned char> uc_vector;

    std::shared_ptr<EVP_CIPHER_CTX> ctx(_("EVP_CIPHER_CTX_new",
                                          EVP_CIPHER_CTX_new()),
                                        EVP_CIPHER_CTX_free);

    uc_vector uc_key(key.begin(), key.end());
    uc_vector uc_iv(iv.begin(), iv.end());
    _("EVP_DecryptInit_ex",
      EVP_DecryptInit_ex(ctx.get(), cipher, nullptr,
                         &uc_key[0], &uc_iv[0]));

    uc_vector uc_ciphertext(ciphertext.begin(), ciphertext.end());
    int len = ciphertext.size() + EVP_CIPHER_block_size(cipher);
    uc_vector plaintext(len);
    _("EVP_DecryptUpdate",
      EVP_DecryptUpdate(ctx.get(), &plaintext[0], &len,
                        &uc_ciphertext[0], uc_ciphertext.size()));

    int tmplen;
    // we expect failures as we try to decrypt using different keys
    if (!EVP_DecryptFinal_ex(ctx.get(), &plaintext[len], &tmplen)) {
      return std::string();
    }

    plaintext.resize(len + tmplen);
    return std::string(plaintext.begin(), plaintext.end());
  }
  // end::aes[]

  std::string random_bytes(int size) {
    std::vector<unsigned char> buf(size);
    RAND_bytes(&buf[0], size);
    return std::string(buf.begin(), buf.end());
  }
}

// POSIX functions
namespace posix {
  int _(const std::string &op, int result) {
    if (result < 0) {
      std::cerr << op << " failed with the error code "
                << errno << " (" << strerror(errno) << ")" << std::endl;
      exit(errno);
    }
    return result;
  }

  bool wait(const std::string &system, int fd) {
    timeval timeout = {};
    timeout.tv_sec = 1;

    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET(fd, &rfds);
    auto status = select(fd + 1, &rfds, nullptr, nullptr, &timeout);
    if (status < 0 && errno != EINTR) {
      _(system + ": select", status);
    }
    return status != 0 && is_listening;
  }

  // POSIX socket functions
  namespace socket {
    inline void reuse_addr(int sock) {
      int reuse = 1;
      _("SRV: SO_REUSEADDR setsockopt",
        setsockopt(sock, SOL_SOCKET, SO_REUSEADDR,
                   &reuse, sizeof(reuse)));
    }

    inline void add_membership(int sock) {
      ip_mreq mreq = {};
      mreq.imr_multiaddr.s_addr = MADDR;
      mreq.imr_interface.s_addr = INADDR_ANY;
      _("SRV: IP_ADD_MEMBERSHIP setsockopt",
        setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                   &mreq, sizeof(mreq)));
    }

    inline void bind(int sock) {
      sockaddr_in addr = {};
      addr.sin_family = AF_INET;
      addr.sin_addr.s_addr = INADDR_ANY;
      addr.sin_port = htons(PORT);
      _("SRV: bind", bind(sock, (const sockaddr *)&addr, sizeof(addr)));
    }

    inline void ttl(int sock, u_char ttl) {
      _("CLT: IP_MULTICAST_TTL setsockopt",
        setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl)));
    }

    void send(int sock, const std::string& msg) {
      if (verbose) {
        std::cout << "-> " << msg << std::endl;
      }

      sockaddr_in addr = {};
      addr.sin_family = AF_INET;
      addr.sin_addr.s_addr = MADDR;
      addr.sin_port = htons(PORT);
      _("CLT: sendto", sendto(sock, msg.c_str(), msg.size(), 0,
                              (const sockaddr *)&addr, sizeof(addr)));
    }
  }
}

void process_register_message(const std::shared_ptr<EVP_PKEY> &pkey,
                              const std::vector<std::string> &tokens) {
  auto name = base64::decode(tokens[1]);
  auto payload = tokens[2];
  if (verbose) {
    std::cout << "<- REGISTER " << name
              << " KEY: " << payload << std::endl;
  }

  auto decoded_str = base64::decode(payload);
  if (verbose) {
    string::dump("<- Raw public key:", decoded_str);
  }

  auto derived_key = crypto::derive_key(pkey, decoded_str);
  if (!keys.count(name)) {
    keys[name] = derived_key;
    resend_public_key = true;
  } else {
    auto old_key = keys[name];
    if (old_key != derived_key) {
      keys[name] = derived_key;
      resend_public_key = true;
    }
  }
}

void process_payload_message(const std::vector<std::string> &tokens,
                             const EVP_CIPHER *cipher) {
  auto name = base64::decode(tokens[1]);
  auto iv = tokens[2];
  if (verbose) {
    std::cout << "<- MESSAGE " << name
              << " IV: " << iv << std::endl;
  }

  for (auto i = 3; i < tokens.size(); ++i) {
    auto payload = tokens[i];
    if (verbose) {
      std::cout <<"<- PAYLOAD: " << payload << std::endl;
    }
    auto decoded_payload = base64::decode(payload);
    auto line = encryption ?
      crypto::decrypt(decoded_payload, cipher, keys[name], base64::decode(iv))
      : decoded_payload;
    if (!line.empty()) {
      std::cout << name << ": " << line << std::endl;
    }
  }
}

void parse_message(const std::shared_ptr<EVP_PKEY> &pkey,
                   const EVP_CIPHER *cipher,
                   const std::string& message) {
  auto tokens = string::split(message, '.');
  if (tokens.size() > 0) {
    auto op = tokens[0];
    if (op == "R") {
      process_register_message(pkey, tokens);
    } else if (op == "M") {
      process_payload_message(tokens, cipher);
    }
  }
}

void process_message(const std::shared_ptr<EVP_PKEY> &pkey,
                     const EVP_CIPHER *cipher,
                     int sock) {
  char buffer[10240] = {};
  auto sz = posix::_("SRV: recvfrom",
                     recvfrom(sock, (char *)buffer, sizeof(buffer),
                              0, nullptr, nullptr));
  if (sz > 0) {
    parse_message(pkey, cipher, buffer);
  }
}

void send_public_key(int sock,
                     const std::string &name,
                     const std::shared_ptr<EVP_PKEY> &pkey) {

  auto public_key = crypto::extract_public_key(pkey);
  auto msg = string::concat("R.", base64::encode(name),
                            ".", base64::encode(public_key));
  posix::socket::send(sock, msg);
}

void listen_messages(const std::shared_ptr<EVP_PKEY> &pkey,
                     const EVP_CIPHER *cipher) {
  std::cout << "Start listening..." << std::endl;

  std::shared_ptr<int>
    sock(new int(posix::_("SRV: socket", socket(AF_INET, SOCK_DGRAM, 0))),
         [](int *fd) { posix::_("SRV: close", close(*fd)); delete fd; });

  posix::socket::reuse_addr(*sock);
  posix::socket::add_membership(*sock);
  posix::socket::bind(*sock);
  listener_ready.notify_all();

  bool resend_public_key = false;
  while (is_listening) {
    if (posix::wait("SRV", *sock)) {
      process_message(pkey, cipher, *sock);
    }
  }

  std::cout << "Stop listening..." << std::endl;
}

void keyboard_interrupt_handler(int s) {
  is_listening = false;
}

void send_input(int sock,
                const std::string& name,
                const EVP_CIPHER *cipher) {
  std::string line;
  std::getline(std::cin, line);
  auto iv = crypto::random_bytes(EVP_CIPHER_iv_length(cipher));

  std::string payload(base64::encode(line));
  if (encryption) {
    payload = "";
    for (const auto &kv: keys) {
      payload += base64::encode(crypto::encrypt(line, cipher, kv.second, iv));
      payload += ".";
    }
  }

  auto msg = string::concat("M.", base64::encode(name),
                            ".", base64::encode(iv),
                            ".", payload);
  posix::socket::send(sock, msg);
}

std::string read_name() {
  std::string name;
  std::cout << "Enter your name: ";
  std::getline(std::cin, name);
  return name;
}

inline void wait_listener() {
  std::mutex mtx;
  std::unique_lock<std::mutex> lck(mtx);
  listener_ready.wait(lck);
}

void run_tests() {
  auto tokens = string::split("A.B", '.');
  assert(tokens.size() == 2);
  assert(tokens[0] == "A");
  assert(tokens[1] == "B");

  assert(base64::encode("abc") == "YWJj");
  assert(base64::decode("bmFtZQ==") == "name");

  assert(string::concat("1") == "1");
  assert(string::concat("1", "2") == "12");

  auto key = "12344567890123456789012";
  auto iv = "1234567890123456";
  auto cipher = EVP_aes_256_cbc();
  auto encrypted = crypto::encrypt("test", cipher, key, iv);
  assert(crypto::decrypt(encrypted, cipher, key, iv) == "test");
}

void prepare(int argc, char *argv[]) {
  run_tests();

  int opt;
  while ((opt = getopt(argc, argv, "ve")) != -1) {
    switch (opt) {
    case 'v':
      verbose = true;
      break;
    case 'e':
      encryption = true;
      break;
    }
  }
}

int main(int argc, char *argv[]) {
  std::shared_ptr<void> crypto_strings([]() {
    ERR_load_crypto_strings(); return nullptr;
  } (), [](void*) { ERR_free_strings(); });

  prepare(argc, argv);
  auto name = read_name();

  auto cipher = EVP_aes_256_cbc();
  if (verbose) {
    std::cout << "Encryption cipher: "
              << EVP_CIPHER_name(cipher) << std::endl;
  }

  auto pkey = crypto::generate_pair(NID_X9_62_prime256v1);
  auto public_key = crypto::extract_public_key(pkey);
  keys[name] = crypto::derive_key(pkey, public_key);

  std::thread listening_thread(listen_messages, pkey, cipher);
  wait_listener();
  signal(SIGINT, keyboard_interrupt_handler);

  std::shared_ptr<int>
    sock(new int(posix::_("CLT: socket", socket(AF_INET, SOCK_DGRAM, 0))),
         [](int *fd) { posix::_("CLT: close", close(*fd)); delete fd; });
  posix::socket::ttl(*sock, 2);
  send_public_key(*sock, name, pkey);

  while (is_listening) {
    const auto STDIN = 0;
    if (posix::wait("CLT", STDIN)) {
      send_input(*sock, name, cipher);
    }
    if (resend_public_key) {
      send_public_key(*sock, name, pkey);
      resend_public_key = false;
    }
  }
  listening_thread.join();

  return EXIT_SUCCESS;
}
