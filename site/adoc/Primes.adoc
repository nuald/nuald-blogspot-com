= Primes generation (algorithms vs real code)
:source-highlighter: rouge

Primes is a quite useful tool and have many applications. They are an essential part
of RSA cryptography, and while elliptic curve algorithms are gaining dominance nowadays,
RSA is still widely used. Plus there are other applications like rolling hash functions, i.e.
assigning an unique prime to each character in the required alphabet and multiplying them
(and implementing rolling by dividing to the number corresponding to the previous character,
and multiplying to the number corresponding to the new character).

There are several algorithms (or even families of algorithms) to calculate primes,
and recently I've stumbled upon a new one:
https://doi.org/10.1016/j.eij.2020.05.002[A new explicit algorithmic method for generating the prime numbers in order].
It's been looking promising, and I've programmed it, but unfortunately it wasn't fast enough
as the real code. Authors compared it with sieves of Eratosthenes and Sundaram using their unified framework,
and got nice results.

However the implementations do not necessarily fit the frameworks. As actual software
developers we want to have fast code now, that works on the real computers with
all their limitations. And that's a reason why nice algorithms are not always
the best, e.g. algorithms utilizing linked lists could be not faster
than using vectors/arrays as lists could lead to memory fragmentation and CPU cache misses.

Nevertheless, I've decided to make a performance comparison, and would like to present to
you the code snippets and the numbers I've got. First, let me describe the algorithms
I've used:

 - Proposed Algorithm (based on the aforementioned article). No optimizations are done except
 breaking the loop when the generated prime index is more than the limit.
 - https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes[Sieve of Eratosthenes]. Used as is.
 - https://en.wikipedia.org/wiki/Sieve_of_Sundaram[Sieve of Sundaram]. Used as is.
 - https://en.wikipedia.org/wiki/Sieve_of_Atkin[Sieve of Atkin]. Optimized to avoid
 additional calculations (please see details in https://www.geeksforgeeks.org/sieve-of-atkin/)

The performance results are presented on the image below. Sieve of Atkin is a winner (however,
I have to admit that first I've tried the implementation presented in the original paper,
and it was quite slow).

image::primes.png[]

Each implementation consist of several parts:

 - allocating specialized `std::vector<bool>` container for the intermediate data
 (memory consumption is O(N) for all algorithms);
 - run the actual algorithm;
 - create the final `std::vector<int>` with the list of primes (the code is similar
 for each implementation, thus none of them got an advantage here).

Proposed Algorithm:

[source,cpp]
----
include::primes.C[tag=proposed_algo]
----

Sieve of Eratosthenes:

[source,cpp]
----
include::primes.C[tag=sieve_of_eratosthenes]
----

Sieve of Sundaram:

[source,cpp]
----
include::primes.C[tag=sieve_of_sundaram]
----

Sieve of Atkin:

[source,cpp]
----
include::primes.C[tag=sieve_of_atkin]
----

As you could see, algorithm run time complexity doesn't necessarily directly correlate to its
implementation performance. Please keep the open mind, and verify the algorithms
using the real code (at least on PoC level). Happy hacking!
