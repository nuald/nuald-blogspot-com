#include <cassert>
#include <chrono>
#include <iostream>
#include <vector>

#include "TGraph.h"
#include "TStyle.h"
#include "TMultiGraph.h"
#include "TAxis.h"
#include "TVirtualPad.h"

namespace chrono = std::chrono;

// tag::proposed_algo[]
std::vector<int> proposed_algo(int limit) {
  std::vector<bool> prime(limit + 1, false);
  for (auto i = 1; i < 1 + (limit - 1) / 2; ++i) {
    prime[2 * i + 1] = true;
  }
  auto k = 1, j = 1;
  const auto sqr = [](int i) { return i * i; };
  const auto max_n = [limit, sqr](int i) {
    return (limit - sqr(2 * i + 1)) / (4 * i + 2);
  };

  while (k > 0) {
    k = max_n(j++);
  }
  k = j;
  for (auto i = 1; i < k + 1; ++i) {
    auto d = max_n(i - 1);
    for (auto n = 0; n < d; ++n) {
      auto index = (2 * i + 1) * (2 * i + 2 * n + 1);
      if (index > limit) {
        break;
      }
      prime[index] = false;
    }
  }

  std::vector<int> result({2});
  for (auto p = 3; p <= limit; ++p) {
    if (prime[p]) {
      result.push_back(p);
    }
  }
  return result;
}
// end::proposed_algo[]

// tag::sieve_of_eratosthenes[]
std::vector<int> sieve_of_eratosthenes(int limit) {
  std::vector<bool> prime(limit + 1, true);

  for (auto p = 2; p * p <= limit; ++p) {
    if (prime[p]) {
      for (auto i = p * p; i <= limit; i += p) {
        prime[i] = false;
      }
    }
  }

  std::vector<int> result;
  for (auto p = 2; p <= limit; ++p) {
    if (prime[p]) {
      result.push_back(p);
    }
  }
  return result;
}
// end::sieve_of_eratosthenes[]

// tag::sieve_of_sundaram[]
std::vector<int> sieve_of_sundaram(int limit) {
  const auto k = (limit - 2) / 2;
  std::vector<bool> marked(k + 1, true);

  for (auto i = 1l; i < k + 1; ++i) {
    for (auto j = i; (i + j + 2 * i * j) <= k; ++j) {
      marked[i + j + 2 * i * j] = false;
    }
  }

  std::vector<int> result({2});
  for (auto i = 1; i < k + 1; ++i) {
    if (marked[i]) {
      result.push_back(2 * i + 1);
    }
  }
  return result;
}
// end::sieve_of_sundaram[]

// tag::sieve_of_atkin[]
std::vector<int> sieve_of_atkin(int limit) {
  std::vector<bool> prime(limit + 1, false);

  for (auto x = 1; x * x < limit; ++x) {
    for (auto y = 1; y * y < limit; ++y) {
      auto n = (4 * x * x) + (y * y);
      if (n <= limit && (n % 12 == 1 || n % 12 == 5)) {
        prime[n] = !prime[n];
      }

      n = (3 * x * x) + (y * y);
      if (n <= limit && n % 12 == 7) {
        prime[n] = !prime[n];
      }

      n = (3 * x * x) - (y * y);
      if (x > y && n <= limit && n % 12 == 11) {
        prime[n] = !prime[n];
      }
    }
  }

  for (auto r = 5; r * r < limit; ++r) {
    if (prime[r]) {
      for (auto i = r * r; i < limit; i += r * r) {
        prime[i] = false;
      }
    }
  }

  std::vector<int> result({2, 3});
  for (auto p = 5; p <= limit; ++p) {
    if (prime[p]) {
      result.push_back(p);
    }
  }
  return result;
}
// end::sieve_of_atkin[]

struct Timing {
  long proposed_algo, sieve_of_eratosthenes, sieve_of_sundaram, sieve_of_atkin;
};

Timing measure(int m) {
  auto start_f = chrono::steady_clock::now();
  const auto f_result = proposed_algo(m).size();

  auto start_e = chrono::steady_clock::now();
  const auto e_result = sieve_of_eratosthenes(m).size();

  auto start_s = chrono::steady_clock::now();
  const auto s_result = sieve_of_sundaram(m).size();

  auto start_a = chrono::steady_clock::now();
  const auto a_result = sieve_of_atkin(m).size();

  auto end = chrono::steady_clock::now();
  assert(f_result == e_result && e_result == s_result && s_result == a_result);
  return Timing {
    chrono::duration_cast<chrono::milliseconds>(start_e - start_f).count(),
    chrono::duration_cast<chrono::milliseconds>(start_s - start_e).count(),
    chrono::duration_cast<chrono::milliseconds>(start_a - start_s).count(),
    chrono::duration_cast<chrono::milliseconds>(end - start_a).count()
  };
}

void primes() {
  const auto iterations = 100;
  const auto step = 1'000'000;
  std::vector<int> x(iterations);
  std::vector<int> y_proposed_algo(iterations);
  std::vector<int> y_sieve_of_eratosthenes(iterations);
  std::vector<int> y_sieve_of_sundaram(iterations);
  std::vector<int> y_sieve_of_atkin(iterations);
  for (auto i = 0; i < iterations; ++i) {
    std::cout << "." << std::flush;
    const auto limit = 10 + i * step;
    const auto& timing = measure(limit);
    x[i] = limit;
    y_proposed_algo[i] = timing.proposed_algo;
    y_sieve_of_eratosthenes[i] = timing.sieve_of_eratosthenes;
    y_sieve_of_sundaram[i] = timing.sieve_of_sundaram;
    y_sieve_of_atkin[i] = timing.sieve_of_atkin;
  }
  std::cout << std::endl;

  auto c = new TCanvas();
  gStyle->SetPalette(kRainBow);

  auto mg = new TMultiGraph();
  mg->SetTitle("; Upper Bound; Duration, ms");

  auto gr_proposed_algo = new TGraph(iterations, &x[0], &y_proposed_algo[0]);
  gr_proposed_algo->SetTitle("Proposed Algo");
  gr_proposed_algo->SetLineWidth(3);
  mg->Add(gr_proposed_algo);

  auto gr_sieve_of_eratosthenes = new TGraph(iterations, &x[0], &y_sieve_of_eratosthenes[0]);
  gr_sieve_of_eratosthenes->SetTitle("Sieve of Eratosthenes");
  gr_sieve_of_eratosthenes->SetLineWidth(3);
  mg->Add(gr_sieve_of_eratosthenes);

  auto gr_sieve_of_sundaram = new TGraph(iterations, &x[0], &y_sieve_of_sundaram[0]);
  gr_sieve_of_sundaram->SetTitle("Sieve of Sundaram");
  gr_sieve_of_sundaram->SetLineWidth(3);
  mg->Add(gr_sieve_of_sundaram);

  auto gr_sieve_of_atkin = new TGraph(iterations, &x[0], &y_sieve_of_atkin[0]);
  gr_sieve_of_atkin->SetTitle("Sieve of Atkin");
  gr_sieve_of_atkin->SetLineWidth(3);
  mg->Add(gr_sieve_of_atkin);

  mg->GetXaxis()->CenterTitle();
  mg->GetYaxis()->CenterTitle();
  mg->Draw("al plc");

  gPad->BuildLegend(0.1, 0.7, 0.48, 0.9);

  c->Print("primes.png");
}

int main() {
  const auto upper_bound = 100'000'000;
  const auto size = sieve_of_sundaram(upper_bound).size();
  const auto& timing = measure(upper_bound);
  std::cout << "For upper bound " << upper_bound << " we have "
    << size << " prime numbers calculated in:" << std::endl;
  std::cout << "\t" << timing.proposed_algo << " ms by the proposed algo" << std::endl
    << "\t" << timing.sieve_of_eratosthenes << " ms by the sieve of Eratosthenes" << std::endl
    << "\t" << timing.sieve_of_sundaram << " ms by the sieve of Sundaram" << std::endl
    << "\t" << timing.sieve_of_atkin << " ms by the sieve of Atkin" << std::endl;
}
