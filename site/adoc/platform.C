#include <iostream>

#ifndef PLATFORM
  #define PLATFORM "N/A"
#endif

void platform() {
  std::cout << "Platform: " << PLATFORM << std::endl;
  #ifdef __BYTE_ORDER__
    std::cout << "__BYTE_ORDER__\t\t= " << __BYTE_ORDER__ << " (";
    #if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
      std::cout << "__ORDER_BIG_ENDIAN__";
    #elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
      std::cout << "__ORDER_LITTLE_ENDIAN__";
    #else
      std::cout << "unknown";
    #endif
    std::cout << ")" << std::endl;
  #else
    std::cout << "__BYTE_ORDER__ is undefined" << std::endl;
  #endif

  #ifdef __FLOAT_WORD_ORDER__
    std::cout << "__FLOAT_WORD_ORDER__\t= " << __FLOAT_WORD_ORDER__ << " (";
    #if __FLOAT_WORD_ORDER__ == __ORDER_BIG_ENDIAN__
      std::cout << "__ORDER_BIG_ENDIAN__";
    #elif __FLOAT_WORD_ORDER__ == __ORDER_LITTLE_ENDIAN__
      std::cout << "__ORDER_LITTLE_ENDIAN__";
    #else
      std::cout << "unknown";
    #endif
    std::cout << ")" << std::endl;
  #else
    std::cout << "__FLOAT_WORD_ORDER__ is undefined" << std::endl;
  #endif

  std::cout << "sizeof(bool)\t\t= " << sizeof(bool)
    << " (" << 8 * sizeof(bool) << " bits)" << std::endl;
  std::cout << "sizeof(char)\t\t= " << sizeof(char)
    << " (" << 8 * sizeof(char) << " bits)" << std::endl;
  std::cout << "sizeof(wchar_t)\t\t= " << sizeof(wchar_t)
    << " (" << 8 * sizeof(wchar_t) << " bits)" << std::endl;
  std::cout << "sizeof(short)\t\t= " << sizeof(short)
    << " (" << 8 * sizeof(short) << " bits)" << std::endl;
  std::cout << "sizeof(int)\t\t= " << sizeof(int)
    << " (" << 8 * sizeof(int) << " bits)" << std::endl;
  std::cout << "sizeof(long)\t\t= " << sizeof(long)
    << " (" << 8 * sizeof(long) << " bits)" << std::endl;
  std::cout << "sizeof(long long)\t= " << sizeof(long long)
    << " (" << 8 * sizeof(long long) << " bits)" << std::endl;
  std::cout << "sizeof(float)\t\t= " << sizeof(float)
    << " (" << 8 * sizeof(float) << " bits)" << std::endl;
  std::cout << "sizeof(double)\t\t= " << sizeof(double)
    << " (" << 8 * sizeof(double) << " bits)" << std::endl;
  std::cout << "sizeof(void*)\t\t= " << sizeof(void*)
    << " (" << 8 * sizeof(void*) << " bits)" << std::endl;
}

int main() {
  platform();
}
